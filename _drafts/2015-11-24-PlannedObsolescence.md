---
layout: post
title: "Planned Obsolescence (Kathleen Fitzpatrick)"
--- 

**Favorite citation and notes:** from the book: Kathleen Fitzpatrick,
*Planned obsolescence : publishing, technology, and the future of the
academy*, New York University Press, 2011


## Introduction: Obsolescence** (p. 1-14)


>  "we in the humanities must move beyond our singular
focus on ink-on-paper to understand and take advantage of
pixels-on-screens"

> 5
 

> "Working on MediaCommons has taught me several things that I mostly
> knew already, but hadn’t fully internalized: first, any software
> development project will take far longer than you could possibly
> predict at the outset; and second, and most important, no matter how
> slowly such software development projects move, the rate of change
> within the academy is positively glacial in comparison."

> 9

## Chap. 1: Peer Review (p. 15-47)

Peer review is indissociable from science making, it is applied
everywhere (Publication, grant, fellowship, position).  The author
stresses that traditional (closed) *peer review* is not adequate for
digital publication and start to explore the concept of peer review.
In the subchapter "Traditional Peer Review and Its Defenses" she
argues that closed peer review is preferred because its assure power
and prestige. 

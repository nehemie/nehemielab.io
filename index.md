---
layout: default
title: Home
---

# Welcome to my research home page

My name is Néhémie Strupler. I grew up and studied mainly in regions
close to the [Rhine River](https://en.wikipedia.org/wiki/Rhine). Design
has long fascinated me, and my interest in understanding the [“human
nature”](https://plato.stanford.edu/entries/human-nature/) led me to
start a bachelor degree in art history, where I discovered that
archaeology and the study of mundane artifacts would be [a right
fit](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0002-introduction-to-computational-thinking-and-data-science-fall-2016/lecture-videos/lecture-9-understanding-experimental-data/)
for me. My current research is on incorporating archaeological information into
computational science for new
[spatiotemporal](https://en.wiktionary.org/wiki/spatiotemporal)
applications (such as  Geographic Information System).  For this, I use
statistical modelling techniques in [R](https://www.r-project.org/) and
most of the time, I work with data collected on prehistoric Western Asia
(the ["Ancient Near
East"](https://oi.uchicago.edu/research/computer-laboratory/ancient-near-east-site-maps)),
particularly on Turkey.  My PhD thesis investigated how the activities
of the capital city of [Ḫattuša](https://www.wikidata.org/wiki/Q181007)
changed in relation to the political organisation of the Hittite state
during the second Millennium BCE.


## Interested?

On this page, you will find information about my research and interests
organised in the following categories:

  - [Blog posts](/posts)

  - [Publications](/publications) (with underlying data)

<!--  - [Research interests](/research) -->
  - [Me](/zabout)


## Otherwise

You can be redirected to my accounts at:
   <ul>
   <li> <p>
   <a href="https://hal.archives-ouvertes.fr/search/index/q/*/contributorId_i/{{ site.author.hal }}"> HAL (manuscript green repository)  </a>
    </p> </li>

   <li> <p>
   <a href="https://orcid.org/{{ site.author.orcid }}"> ORCiD </a>
   </p></li> 

   <li> <p>
   <a href="https://gitlab.com/{{ site.author.gitlab }}">
    GitLab
   </a></p> 

   </li>
<li> <del>academia.edu</del>   (deleted due to the aggressiv marketing)</li>
   </ul>

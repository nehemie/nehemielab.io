This is the source of the personal website of Néhémie Strupler

It was started by following a post of [Joshua
Lande](http://joshualande.com/jekyll-github-pages-poole/), using
first [poole](https://github.com/poole/poole) and evolved to
[hyde](https://github.com/poole/hyde).  Rendering `.Rmd` posts
for  [Jekyll](http://jekyllrb.com/) was first done via the
[servr](http://cran.r-project.org/web/packages/servr/index.html)
package (based on a note from [Sahir
Bhatnagar](http://sahirbhatnagar.com)). Currently it is rendered
via  `R -e "blogdown::build_site()"` (thank you [Yihui, Amber, &
Alison](https://bookdown.org/yihui/blogdown/jekyll.html)). I
serve
it with `jekyll serve` on the world wide web thanks to [`GitLab
CI`](.gitlab-ci.yml) and [GitLab
pages](https://docs.gitlab.com/ee/user/project/pages/).


## Some tricks

### See Jekyll site

With the shell from Jekyll site's root directory, run the command

```bash
$ jekyll serve
```

The config files for R are in [/R/](R/)

### Tags 

from <http://www.minddust.com/post/tags-and-categories-on-github-pages/>

How to add this feature?  add `blog_by_tag` in `_layouts`, map the
tag in `_data/tags.yml` and add pages in `blog/tag/,,,`

### Licence:

Text: CC BY-SA (http://creativecommons.org/licenses/by-sa/4.0/)

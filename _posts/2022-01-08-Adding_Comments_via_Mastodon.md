---
layout: post
title: Hello World of 2022
web_title: "Adding Mastodon Comments to Jekyll Blog"
comments:
  show: true
  fediHost: fosstodon.org
  fediusername: nehemie
  fediid: 107592503149506234
tags: [freesoftware, technical]
---

## Happy New Year!

For whatever reason,[^gemini_fn] I saw [this post][lotta] from
[@dsyates][dsyates] about using [Mastodon] to add comment
functionality to a "static" blog. This is a feature that I was
looking for a long time. However, when I searched for a solution,
I could not find a straightforward way to integrate this feature
without non-FOSS third-party tools.

But, the idea to use Mastodon makes it a funny workflow. To get
comments, you need a Mastodon account and a [toot][toots] about
your post. Each message on Mastodon has a unique ID that you can
link to it. Integrate it with your blogpost in order to collect
the comments on the page of your post. You will need some
templating, CSS, and Javascript, but [@tcheneau][tcheneau] has a
good step by step [post about it][tchenau_steps] (thanks!).

I found it very attractive, because that is encouraging me to use
Mastodon and connect with a new community, now I am here:
[@nehemie][nfstd]!

To conclude, as [@dsyates][dsyates] puts [it][lotta]:

> This blog post is really just a test to try out as implemented
> and explained by [@carl](https://linuxrocks.online/@carl): in
> [this
> blogpost](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/)
> on a Hugo static site; as well as by
> [@xosem](https://toot.site/@xosem): in [this
> blogpost](https://blog.xmgz.eu/jekyll-mastodon-comment) on a
> Jekyll static site.

## Addendum (2022-02-01)

I was playing with the Mastodon platform, and in my preferences,
I went to "Automated post deletion" and selected "Automatically
delete old posts" after a threshold of two weeks. I like hiking
and one of the moto is "leave no trace". I thought that it was
clever to select it (for privacy),  and that it would free space
on the server (saving energy). However, I did not realise that it
would delete my tooth about my blogpost that is referenced as ID!
Therefore, I succeeded in deleting the first comments on my blog
(I am sorry about that). So if you add comment via Mastodon,
either do not auto-delete your post, or make sure to activate an
option to avoid this (such as bookmarking to prevent deletion).

## References

[^gemini_fn]: I think that I was researching about the [Gemini protocol][gemini_protocol] and landed on <https://lottalinuxlinks.com>.

[lotta]: https://lottalinuxlinks.com/using-mastodon-for-comments-on-a-static-blog/

[gemini_protocol]: https://gemini.circumlunar.space/

[Mastodon]: https://joinmastodon.org/

[toots]: https://docs.joinmastodon.org/user/posting/

[dsyates]: https://fosstodon.org/@dsyates

[nfstd]: https://fosstodon.org/@nehemie

[tcheneau]: https://linuxrocks.online/@tcheneau

[tchenau_steps]: https://amnesiak.org/post/2021/01/30/hugo-blog-with-mastodon-comments-extended/

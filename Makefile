site: validate
	R -e "blogdown::build_site()"
	jekyll serve

VNU:="/home/usr/libs/vnu/vnu-runtime-image/bin/vnu --stdout --format text"
VNU_LOG:="log_val.txt"
SITE:="./_site"
validate:
	"${VNU}"  --skip-non-html "${SITE}" > "${VNU_LOG}"
	"${VNU}"  --skip-non-css "${SITE}" >> "${VNU_LOG}"
	"${VNU}"  --skip-non-svg "${SITE}" >> "${VNU_LOG}"

---
layout: page
title: About
---

![Portrait]({{ site.baseurl }}/media/2014-10-8--Pingu.jpg "Artwork from Vera Egbers")

I completed in September 2016 a PhD in Archaeology ([University of
Strasbourg](http://www.unistra.fr/index.php?id=english), France jointly with
[University of Münster](https://uni-muenster.de/en/), Germany) on the Hittites'
kingdom capital Boğazköy / Ḫattuša entitled "The Lower City of Boğazköy during
the Second Millennium BC: Political & Urban restructuring into a Kingdom’s
Capital." I examined the evolution of social patterns of the domestic quarters,
at the critical moment when the site became the political capital of the
Hittites. 

I am particularly interested in developing theory and methods for exploring
archaeological, historical and geographical data together and making an open,
transparent and reproducible research. Most of my work is written with
[R](http://r-project.org/) using [Rmarkdown](http://rmarkdown.rstudio.com/) or a
derivative.

Have questions or suggestions? Feel free to [ask me via
mail](mailto:nehemie.strupler@ifea-istanbul.net). PGP Key [here](/publickey.txt)

Thanks for reading!

*****
Artwork from `Vera Egbers`


---
layout: post
title: "Anadolu’da sarayların ortaya çıkışı"
tags: [archaeology, anatolia]
lang: tr
---

**Cette note est une traduction par Nilda Taşköprü de la note _Apparition du
palais en Anatolie_ rédigée dans le cadre d'une bourse AMI à l'IFEA à Istanbul
et a été publiée sur le site de l'IFEA : http://www.ifea-istanbul.net/ en tant
que présentoir du mois d'octobre 2016**

Önemli bir kişinin, özellikle de bir hükümdarın geniş ve gösterişli ikametgahı
olan saray binaları, karşımıza, mükemmel bir iktidar simgesi ve dönemlere göre
ifade ettiği yeni manalarla çıkar. Fransız Anadolu Araştırmaları Enstitüsüne
gelirken önünden geçilen Fransız Sarayı, yukarıda sözü ettiğimiz ihtişam
konusunu gayet güzel vurgulamakla birlikte, İstanbul’daki sayısız örnekten
(Büyük Saray, Tekfur, Topkapı, Dolmabahçe, Beylerbeyi ve Yıldız Sarayı) yalnızca
bir tanesidir. Görkemli bir mimari aracığılıyla gösterişçiliği ve işlevselliği
bir araya getiren saray binaları, hem iktidar sahibinin ikametgahı, hem de idari
bir merkezdir. Saray kelimesi, M.S. 15. yüzyıl itibariyle, Fransızcada şehir
mahkemelerinin bulunduğu bina (adalet sarayı) anlamında da kullanılmaya
başlanır. Türkçe ve Fransızcada da benzer ifadelere birçok örnek göstermek
mümkündür; (Cumhurbaşkanlığı Sarayına karşılık Palais de l'Élysée veya adalet
sarayı anlamındaki palais de justice gibi). Ayrıca İngilizcedeki palace
kelimesinin etkisiyle, İstanbul’da isimleri içinde saray kelimesi geçen lüks
oteller gibi; Simit Sarayı, Kahve Sarayı, ve Saray Muhallebicisi gibi mağazalar,
kelimenin ticaret sektöründe de sıklıkla kullanıldığını gösterir.

IFEA Kütüphanesinde, saray binalarının ortaya çıktığını söyleyebileceğimiz, Tunç
Çağı (M.Ö. 3000-1200) hakkında ilginç kaynaklara rastlamak mümkündür. Ekonomik
ve toplumsal hayatın saraya dayandığı ve “saray medeniyetleri” diye de
adlandırılan birçok medeniyet, Tunç Çağında ortaya çıkar. Saray, Mezotopamya’da
M.Ö. 3. binyıl itibariyle, Sümer yazısının kullanıldığı metinlerde, “E2-KAL”
kalıbıyla sıklıkla ifade edilmiştir. Bu ifadeye, M.Ö. 2. binyıl ortasında,
Anadolu’daki Kapadokya Pazarları (Garelli 1963) ve Hitit döneminde (Bittel 1976)
rastlanır. Kökenbilim meraklıları için, Sümerce “E2-KAL” kalıbının, Akatçaya
ēkallum olarak geçtiğini ve Arapçadaki هيكل (haykal) kelimesinden de Türkçeye
heykel diye aktarıldığını hatırlatalım. Aslen, ikamet edilen yer anlamına gelen
kelime, istiare sonucu buranın tüm sakinlerini ifade eder. Yani eski Ortadoğu’da
saray, yalnızca hükümdar ailesinin yaşadığı mekân olmayıp, ayrıca idari
mercilerin de görevlerini icra ettikleri yerdir. Suriye’nin Deyrizor şehri
yakınlarındaki Mari kentinin kraliyet sarayları[1], günümüze yazılı belgelerle
birlikte ulaşmış örnekler arasında, en çok belgeye sahip olanlarındandır. IFEA
Kütüphanesinde ve www.persee.fr sitesinde (bkz. aşağıdaki kaynaklar), Mari’de
gerçekleştirilmiş kazılar ve burada bulunan arşivler hakkında çok sayıda yayın
mevcuttur. Mari’deki, M.Ö. 2. binyıl dönemine ait kraliyet sarayı, 200 metre
uzunluğunda ve 120 metre genişliğindedir. Sarayın giriş katında bulunan 300 oda,
saray ve kraliyet idaresi ile ambarlara ayrılmış alanları, krala ait odaları,
haremi ve kutsal mekânları birbirinden ayıracak şekilde düzenlenmiştir. Ayrıca
birçok bulgu; günümüze ulaşmasa da, toplam alanı ve oda sayısını ikiye katlayan,
bir üst katı daha akla getirir. Sit alanı, zengin yazıtlarıyla (20 000 kil
tablet) olduğu kadar, günümüze gayet iyi korunarak ulaşmış, resim, heykel ve çok
sayıdaki nesne sayesinde binanın ihtişamı ve faaliyeti hakkında bir fikir verir.
Burası, sarayın aslında, kralın gücünün somutlaştırılarak, iktidarın nasıl
propogandasının yapılıp, ideolojisine hizmet edildiğinin harika bir örneğidir.

Çok sayıda belge ile birlikte günümüze ulaşmış saraylar bakımından, Anadolu
zengin bir coğrafya olarak karşımıza çıkar. İlk kent devletlerinin ortaya
çıktığı M.Ö. 4. binyılın ikinci yarısına (M.Ö. 3500-3000) tarihlenmesinden
ötürü, son derece önem arz eden, Malatya yakınındaki Arslantepe kazılarında, her
sene, sit alanı ile ilgili yeni ayrıntılar gün yüzüne çıkarılır. Bu kazılarda,
pişmemiş tuğladan inşa edilmiş ve yer yer duvarların 2 metre kalınlığa ulaştığı
“saray kompleksi” denen devasa yapı kısmen ortaya çıkarılmıştır. Söz konusu
komplekste, tapınaklar olduğu kadar, idari işlere ayrılmış alan, avlu ve
ambarlar da bulunur. Ayrıca birtakım çanak çömlek ürünlerinin, ilk defa toplu
hâlde imal edilerek, standartlaşmasıyla erzak düzeni kolaylaştırılmıştır. Bu sit
alanı ile ilgili olarak, bir saraydan söz etmek zordur, mesken teşkil edebilecek
hiçbir odaya rastlanmamıştır zira. Bununla birlikte kazılar, yapının yalnızca
bir kısmında yürütülmüştür ve söz konusu dönemin siyasi düzenine dair ayrıntılar
bilinmemektedir. Yine de kil üzerine basılmış resmi mührü kırmadan açmanın
mümkün olmadığı birçok mühürlü kalıntı, egemen sınıfın idaresindeki binayı ve
toprakları nasıl düzenleyip, denetlediği hakkında bir fikir verir. Bu mühürlü
kalıntılar ve “saray kompleksinin” devasa mimarisi, diğer kesimlerden giderek
ayrılan, idareci sıfatındaki egemen sınıfın otoritesinin ve toplumsal konumunun
bir işaretçisidir.

Buna karşılık, M.Ö. 2. binyıl başında Anadolu’da yazının ortaya çıkmasıyla
birlikte, her kentte bir sarayın bulunması sıradanlaşır. “Prens” adı verilen ilk
hükümdarların isimlerine, Orta Tunç Çağına (M.Ö. 2000-1700) ait Kaniş (bugünkü
Kültepe, Kayseri) belgelerinde rastlanır. Buradaki saray yıkıntıları arasından,
Kaniş hükümdarı Warşama’ya hitaben yazılmış bir mektup 1955 yılında bulunmuştur.
“Warşama Sarayı” diye geçen bu sarayda, taş temeller üzerine oturan pişmemiş kil
tuğlalardan örülmüş altmış küsur oda bulunur. Bu yapıdan daha eski başka bir
binanın daha olduğu bilinmektedir ve hâlihazırda F. Kulaksızoğlu’nun
başkanlığında yürütülen çalışmalarda, M.Ö. 3. binyıla tarihlenen, belki de bir
saray olan devasa bir kompleksi gün yüzüne çıkarılmaktadır. “Warşama Sarayı”,
prens ve eşinin ikametgahıdır, fakat sarayda kraliyet ailesi ve hizmetkarların
yanı sıra, bazı üst düzey yetklililer de yaşamıştır. Saray, aynı zamanda Anadolu
topraklarını (dönem itibariyle kent devletleri söz konusudur) işleten idari
merkezdir. Ticaret de doğrudan saraya bağlıdır. Kervanlar birçok vergiye tabidir
ayrıca idareciler, mevkileri dolayısıyla satın almada öncelik hakkını
kullanırlar. “Warşama Sarayı”, en tanınmış örnek olmakla birlikte, idareyle
ilgili tüm işlemlerin bir arada sunulduğu bu türden bir binanın tüm kentlerde
bulunduğu düşünülür. Her ne kadar kralın “tanrıların yardımcısı” rolünü, büyük
ihtimalle Hitit Krallığından önce de taşıdığı düşünülse de, kralın bu rolünden,
ancak Hitit Krallığının Anadolu’da geç Tunç Çağı döneminde ortaya çıkışı
itibariyle kesin şekilde bahsetmek mümkündür. Başkent Hattuşaş’taki (Çorum,
Boğazkale) kraliyet sarayı, birbirine avlu ve revaklar aracılığıyla bağlanan çok
sayıda binanın bir araya gelerek oluşturduğu bir bütünün, geriye kalan
yerleşimden istihkam surlarıyla ayrıldığı ilk saray örneklerinden biridir. Bu
açıdan, Topkapı Sarayının düzeniyle bir benzerlik kurmak mümkündür.

Saraylar, Tunç Çağı sonrasında, ekonomik ve toplumsal hayatın merkezi oldukları
“saray medeniyetlerindeki” yerlerini kaybetseler de kraliyet ikametgahı olmaya
devam eder. Günümüzde de ihtişam ve etkileyiciliklerini koruyan saraylar, bir
medeniyetin hüner ve becerilerine hâlen işaret etse de sarayların, büyük
eşitsizlikleri “meşrulaştıran” bir mantığın ürünü olduğunu yadsımamak gerekir.

Metin: Néhémie Strupler, Eylül 2016
Çeviri: Nilda Taşköprü, Eylül 2016



Genel kaynakça

Aurenche Olivier (Ed.) 1977
Dictionnaire illustré multilingue de l'architecture du Proche-Orient ancien
COLL XLI b 003

Bittel Kurt 1976
Les Hittites
BA 04-04

Forest Jean-Daniel 1996
Mésopotamie, l'apparition de l'Etat VIIe-IIIe Millénaires Or Gén 081

Garelli Paul 1963
Les assyriens en Cappadoce
COLL I B 19

Mari Kraliyet Sarayı

Parrot André 1958
Mission archéologique de Mari : Volume II : Le palais : Architecture
COLL II 068

Parrot André 1958
Mission archéologique de Mari : Volume II : Le palais : Peintures murales
COLL II 069

Parrot André 1959
Mission archéologique de Mari : Volume II : Le palais : Documents et monuments
COLL II 070

Parrot André 1974
Mari capitale fabuleuse
HA Syr Si 02 I

Rouault Olivier 1977
Mukannisum : L' administration et l'économie palatiales à Mari
HA Ass Gén 01 XVIII

MARI Annales de Recherches Interdisciplinaires
COLL XIII A 08 Volume 1 à 7

Arslantepe Saray Kompleksi

Frangipane Marcella 2001
«Arslantepe-Malatya: A Prehistoric and Early Historic Center in Eastern Anatolia», dans Gregory McMahon and Sharon Steadman (éd), The Oxford Handbook of Ancient Anatolia: (10,000-323 BCE) https://dx.doi.org/10.1093/oxfordhb/9780195376142.013.0045

Frangipane Marcella 1997
A 4th Millennium Temple/Palace Complex at Arslantepe-Malatya. North-South Relations and the Formation of Early State Societies in the Northern Regions of Greater Mesopotamiai, Paléorient 23.1: 45–73

Frangipane Marcella (éd.) 2010.
Economic Centralisation in Formative States. The Archaeological Reconstruction of the Economic System in 4th Millennium Arslantepe

Kaniş Sarayı

Cecile Michel 2001
Correspondance des marchands de Kanish au début du IIe millénaire avant J.-C.

Özgüç Tahsin 1986
Kaniş II Eski yakındoğu'nun ticaret merkezinde yeni araştırmalar / New Researches at the Trading Center of the Ancient Near East
HA Ana Si 074

Özgüç Tahsin 2003
Kültepe Kanış/Neşa : The earliest international trade center and the oldest capital city
HA Ana Si 177

Hattuşaş Sarayı

Alp Sedat 1993
Beitrage zur Erforschung des hethitischen Tempels, Kultanlagen im Lichte der Keilschriffexte
HA Ana Gén 200

Bittel Kurt 1970
Hattusha The Capital of the Hittites HA Ana Si 064

Neve Peter 1982
Büyükkale die Bauwerke : Grabungen 1954-1966
HA Ana Si 030

Neve Peter 1992
Hattusa-Stadt der Götter und Tempel : Neue Ausgrabungen in der Haupstadt der Hethiter
HA Ana Si 070

[1] Mari Kraliyet saraylarının hâlihazırdaki durumu ile ilgili, American Association for the Advancement of Science tarafından hazırlanmış, Ağustos 2016 tarihli rapora bkz. Assessing the Status of Syria’s Tentative World Heritage Sites Using High-Resolution Satellite Imagery https://www.aaas.org/page/ancient-history-modern-destruction-assessing-status-syria-s-tentative-world-heritage-sites-7

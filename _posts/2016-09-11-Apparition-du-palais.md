---
layout: post
title: "L'apparition du palais en Anatolie "
tags: [archaeology, anatolia]
lang: fr
---

**Cette note a été rédigée dans le cadre d'une bourse AMI à l'IFEA à Istanbul
et a été publiée sur le site de l'IFEA : http://www.ifea-istanbul.net/ en tant
que présentoir du mois d'octobre 2016**

Symbole par excellence du pouvoir, sans cesse réactualisé, le palais désigne la
demeure vaste et luxueuse d'un personnage important, en particulier la résidence
d'un souverain. Une visite à l'IFEA, où l'on passe devant le Palais de France le
rappelle, mais ce n'est qu'un exemple parmi tant d'autres à Istanbul (Büyük
Saray, Tekfur Sarayı, Topkapı Sarayı, Dolmabahçe Sarayı, Beylerbeyi Sarayı,
Yıldız Sarayı, etc.). Le palais associe l'ostentation du luxe architectural avec
un aspect fonctionnel, car il sert de lieu de résidence et de centre
administratif au pouvoir. Le palais désigne aussi à partir du 15e siècle ap.
J.-C. l'édifice où siègent les différents tribunaux d'une ville (palais de
justice). Ces usages sont tout à fait similaires en français et en turc et de
nombreux parallèles existent (Palais de l'Élysée / Cumhurbaşkanlığı Sarayı,
palais de justice / adalet sarayı). Par ailleurs, l'usage commercial du mot se
rencontre fréquemment à Istanbul où Simit Sarayı, Kahve Sarayı, ou Saray
Muhallebicisi côtoient des hôtels luxueux appelés également _saray_ sous
l'influence de l'anglais _palace_.

La bibliothèque de l'IFEA possède un fonds intéressant pour remonter à
l'apparition du palais, que l'on peut situer à l'âge du Bronze (3000-1200 av.
J.-C.). Cette période qui voit la naissance de nombreuses civilisations que l'on appelle
parfois « civilisations palatiales » où le monde économique et sociale
dépend de la cour. En Mésopotamie, le palais a couramment été désigné  par les
sumerogrammes « E2-KAL » dans les textes dès le IIIe millénaire av. J.-C., un
usage que l'on retrouve en Anatolie au IIe millénaire av. J.-C. lors de la
période des Comptoirs de Cappadoce (Garelli 1963) et du Royaume Hittite (Bittel
1976). Précisons pour les enthousiastes d'étymologie que le sumérien « E2-KAL »
est devenu en akkadien ēkallum et a été transmis en turc sous la forme de
_heykel_ à travers l'arabe  هيكل (haykal). À l'origine, le mot qualifie une
résidence et par métonymie, il sert à désigner l'ensemble des personnes qui y
habitent. Le palais au Proche-Orient ancien n'est donc pas uniquement la résidence de la
famille royale mais il sert également de siège au gouvernement. Les
palais royaux de la ville de Mari (Syrie, Deir ez-Zor)^[Sur la situation
actuelle, on peut consulter le rapport publié en août 2016 _Assessing the Status
of Syria’s Tentative World Heritage Sites Using High-Resolution Satellite
Imagery_ par l'American Association for the Advancement of Science
https://www.aaas.org/page/ancient-history-modern-destruction-assessing-status-syria-s-tentative-world-heritage-sites-7]
font partie des témoignages les mieux documentés, et de très nombreuses
publications sont disponibles à la bibliothèque sur les fouilles et les archives
de Mari, tout comme à travers le portail [Persee.fr](http://www.persee.fr/)
(voir les indications bibliographiques ci-dessous). Au IIe millénaire, le palais royal de Mari 
mesure 200 m par 120 m et abrite 300 pièces au rez-de-chaussée. Ces dernières sont
organisées selon une circulation complexe qui différencie les salles dédiées à
l'administration du palais et du royaume, les zones de stockage de denrées, les
appartements du roi, le harem et les lieux sacrés. En outre, de nombreux indices
permettent d'identifier que ce bâtiment disposait d'un étage à présent détruit, doublant la
surface et le nombre de pièces disponibles. Tant la documentation épigraphique
abondante (quelque 20 000 tablettes d'argiles)  que les vestiges très bien
conservés (peintures, statues et nombreux objets) renseignent sur l'ostentation
du bâtiment et son fonctionnement. C'est un excellent exemple qui montre comment
à son origine, le palais sert à la matérialisation du pouvoir du roi et donc de
soutien à la propagande et à l'idéologie royale.

L'Anatolie n'est cependant pas en reste d'exemples de palais très bien
documentés. Les fouilles d'Arslantepe (à côté de Malatya) révèlent chaque année de
nouveaux détails sur l'un des cas les plus passionnants, puisqu'il date de la 2e
moitié du IVe millénaire (3500-3000 av. J.-C.), une période charnière qui voit
l'apparition des premiers états territoriaux. Les fouilles à Arslantepe ont
révélé une partie d'un bâtiment monumental, appelé « complexe palatial »,
construit en briques crues et dont certains murs mesurent 2 m d'épaisseur. Ce
complexe regroupe tout aussi bien des temples que des zones dédiées à
l'administration, des cours et des pièces de stockage. Par ailleurs, certaines productions 
céramique sont, pour la première fois, produites en masse et
deviennent standardisées pour faciliter la gestion des denrées. Il est difficile
de parler de palais, puisque aucune pièce pour la résidence n'est connue (mais
seule une partie du bâtiment a été fouillée) et les détails de l'organisation
politique à cette époque sont inconnus.  Néanmoins, les vestiges de nombreux
scellés (masse d'argile couverte de sceaux officiels, de manière à ce qu'on ne
puisse pas procéder à une ouverture sans briser les cachets), donnent une idée
de l'organisation et du contrôle exercés par l'élite pour la gestion du bâtiment
et du territoire qu'elle administrait. Ces scellés et l'aspect monumental de
l'architecture du « complexe palatial »  témoignent de l'autorité et de la
position sociale de l'élite dirigeante qui se démarque de plus en plus du commun
des mortels. 


En revanche, avec l'apparition de l'écriture en Anatolie au début du IIe
millénaire, le palais va devenir une constante dans les villes. Les premiers
noms de rois (appelés « princes »)  apparaissent dans les documents du Bronze
Moyen (2000-1700) de Kaniš (moderne Kültepe, à côté de Kayseri). Là, dans les
décombres du palais fut découverte en 1955 une lettre adressée à Waršama, prince
de Kaniš. Ce palais dit « palais de Waršama », regroupe une soixantaine de
pièces, construites de murs en briques d’argile crue reposant sur des
fondations en pierres. On connaît également un palais antérieur et les travaux
menés actuellement par F. Kulakoğlu sont en train de mettre au jour un complexe
monumental du IIIe millénaire, qui pourrait bien être un palais. Le « palais de
Waršama » est la résidence du couple princier, mais il abritait aussi un certain
nombre de hauts dignitaires en plus de la famille royale et de la domesticité.
C'est aussi le siège de l’administration anatolienne chargée de la gestion du
territoire (on parle le plus souvent alors de cité-état). En outre, le palais
est directement impliqué dans l'activité commerciale. Les caravanes sont redevables de
plusieurs taxes, et les autorités usent de leurs prérogatives en matière de
droits de préemption. Le « palais de Waršama » est l'exemple le mieux connu,
mais toutes les villes devaient posséder un tel bâtiment qui regroupait toutes
les fonctions qu'exerçait le souverain. C'est seulement avec l'apparition du
royaume hittite au Bronze récent que l'on peut affirmer que le roi endosse
aussi le rôle de « vicaire des dieux », même s'il est fort probable que cela était
déjà le cas auparavant. Le palais royal de la capitale Hattuša
(Boğazkale, Çorum) est l'un des premiers exemples où le palais correspond à
un ensemble de plusieurs bâtiments distincts, réunis par des cours et des
portiques, l'ensemble étant clairement séparé du reste de l'habitat par un mur
de fortification; un schéma qui ressemble à celui de Topkapı. 


À la suite de l'Âge du Bronze, le palais ne tiendra plus la place centrale qu'il
avait dans les civilisations de l'âge du Bronze, où il était le centre de la vie
économique et sociale, même si, en tant que résidence royale, le palais ne
disparaîtra jamais. Néanmoins, si le luxe et le raffinement des palais
impressionnent encore aujourd'hui et sont des témoins de l'ingéniosité d'une
civilisation, il ne faut pas omettre que le palais est avant tout le résultat
d'une idéologie destinée à « justifier » des inégalités exacerbées.

### Généralités

Aurenche Olivier (Ed.) 1977 	
Dictionnaire illustré multilingue de l'architecture du Proche-Orient ancien 	
COLL XLI b 003/

Bittel Kurt	1976 	
Les Hittites 	
BA 04-04

Forest Jean-Daniel 1996 	
Mésopotamie, l'apparition de l'Etat VIIe-IIIe Millénaires 	
Or Gén 081

Garelli Paul 1963 	
Les assyriens en Cappadoce 	
COLL I B 19

### Palais royal de Mari


Parrot André 1958 	
Mission archéologique de Mari : Volume II : Le palais : Architecture 	
COLL II 068

Parrot André 1958 	
Mission archéologique de Mari : Volume II : Le palais : Peintures murales 	
COLL II 069
	
Parrot André 1959 	
Mission archéologique de Mari : Volume II : Le palais : Documents et monuments 	
COLL II 070
	
Parrot André 1974 	
Mari capitale fabuleuse 	
HA Syr Si 02 I

Rouault Olivier 1977 	
Mukannisum : L' administration et l'économie palatiales à Mari

HA Ass Gén 01 XVIII  	
MARI Annales de Recherches Interdisciplinaires  	
COLL XIII A 08 Volume 1 à 7 


### Complexe "palatial" d'Arslantepe

Frangipane Marcella 2001 	
«Arslantepe-Malatya: A Prehistoric and Early Historic Center in Eastern Anatolia»,
 dans Gregory McMahon and Sharon Steadman (éd), 
The Oxford Handbook of Ancient Anatolia: (10,000-323 BCE)
https://dx.doi.org/10.1093/oxfordhb/9780195376142.013.0045

Frangipane Marcella 1997, 	
A 4th Millennium Temple/Palace Complex at Arslantepe-Malatya.
North-South Relations and the Formation of Early State Societies in the Northern
Regions of Greater Mesopotamiai, Paléorient 23.1: 45–73

Frangipane Marcella (éd.) 2010. 	
Economic Centralisation in Formative States. The Archaeological
Reconstruction of the Economic System in 4th Millennium Arslantepe


### Palais de Kaniš

Cecile Michel 2001 	
Correspondance des marchands de Kanish au début du IIe millénaire avant J.-C. 

Özgüç Tahsin 1986 	
Kaniş II Eski yakındoğu'nun ticaret merkezinde yeni araştırmalar / New Researches at the Trading Center of the Ancient Near East 	
HA Ana Si 074 

Özgüç Tahsin 2003 	
Kültepe Kanış/Neşa : The earliest international trade center and the oldest capital city 	
HA Ana Si 177
	

### Palais de Hattuša et de Tapigga

Alp Sedat 1993 	
Beitrage zur Erforschung des hethitischen Tempels, Kultanlagen im Lichte der Keilschriffexte  	
HA Ana Gén 200/

Alp Sedat 1991 	
Hethitische Briefe aus Maşat-Höyük 	
HA Ana Si 163

Bittel Kurt 1970 	
Hattusha The Capital of the Hittites 	
HA Ana Si 064

Neve Peter 1982 	
Büyükkale die Bauwerke : Grabungen 1954-1966 	
HA Ana Si 030

Neve Peter 1992 	
Hattusa-Stadt der Götter und Tempel : Neue Ausgrabungen in der Haupstadt der Hethiter 	
HA Ana Si 070

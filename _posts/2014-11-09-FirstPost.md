---
layout: post
title: First Post
---

This my first post. For this post there is no other purpose than to test how it
works. Much thanks to [Joshua Lande](http://joshualande.com/) and [his
repository](http://joshualande.com/jekyll-github-pages-poole/) using
[poole](https://github.com/poole/poole), which I just forked. I will use this
site as an open sandbox to learn how to use and maintain an open and
reproducible microblogging site.

### Why?

Since one year I am following [R-bloggers](http://www.r-bloggers.com/) and I
love learning from the ideas aggregated from all the blogs. After a close
reading of the book [Opening Science](http://book.openingscience.org/), and
enjoying so much input from [R-bloggers], I decided to give a try and to have my
own microblogging site.

I hope I will use this blog to :

  - Explain on-going thoughts about my research
  - Share ideas and papers
  - Link or publish and explain some of my data
  - Commit myself to transparent research

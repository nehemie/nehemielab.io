---
layout: post
title: "Comment j'écris ma thèse"
tag: [openaccess, freesoftware]
lang: fr
...

Lors de la rencontre du Collège Doctoral Trinational FISA-MIAG, j'ai fait une
petite présentation sur la transmission du savoir et sur les décisions que j'ai
prises pour écrire ma thèse (et transmettre mon savoir acquis de la meilleure
façon).  Après avoir mis en garde sur les dangers des *black boxes* et des
problèmes du conservatisme académique, j'ai exploré le concept « d'Humanités
numériques / digitales » pour ouvrir le débat sur les questions que cela pose à
des doctorants qui ont 40 ans -- soyons optimistes -- de carrière devant eux.
J'en ai profité pour présenter les principes pour ma thèse : 

  - mise à disposition des données (Zenodo) ; 

  - transparence et reproductibilité des analyses grâce à R.

  Voici le lien vers [la
présentation](https://nehemie.github.io/slideshow/MyWorkflow) et le [code
source](https://github.com/nehemie/slideshow/tree/master/2015-02-21--MyWorkflow)


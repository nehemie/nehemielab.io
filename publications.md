---
layout: publications
title: Publications
web_title: Publications of Néhémie Strupler
---

<!-- 
### Peer-reviewed Monograph (and Underlying Data)

#### 2021


Die Ausgrabungen in der Unterstadt I. Auswertung der bronzezeitlichen Besiedlung auf der Westterrasse (1935-1978).  Fouilles archéologiques de la ville basse I. Analyse de l'occupation du l'âge du Bronze de la Westterrasse (1935-1978)
:    (in French with a 40 p. summary in German and Turkish). Publisher : Deutsches Archäologisches Institut

*Open Archive*: Boğazköy-Ḫattuša, Die  Ausgrabungen in der Unterstadt I. Anhang Archivdaten
:    IANUS - Forschungsdatenzentrum\
    _`Open access DOI:` [10.13149/mmfm-ka36](https://doi.org/10.13149/mmfm-ka36)_

*Reproducible Code*: Boğazköy-Ḫattuša, Die Ausgrabungen in der Unterstadt I. Digitale Beilage
:    IANUS - Forschungsdatenzentrum\
    _`Open access DOI:` [10.13149/mmfm-ka36-2](https://doi.org/10.13149/mmfm-ka36-2)_

-->

## Articles

### 2021

{% assign reference = site.data.strupler_pubs.Strupler2021_rediscover %}
{% include publication_article.html %}

{% assign reference = site.data.strupler_pubs.Strupler2021_suba %}
{% include publication_article.html %}

{% assign reference = site.data.strupler_pubs.Strupler2021_sealing %}
{% include publication_article.html %}

{% assign reference = site.data.strupler_pubs.Strupler2021_aaC14 %}
{% include publication_article.html %}


### 2017

#### Reproducibility in the field: transparency, version control and collaboration on the Project Panormos Survey (with T.C.  Wilkinson)

Open Archaeology 3, 219-304.

 -  _`Open access DOI:` [10.1515/opar-2017-0019][repro-inthe-field]_

#### Open Science in Archaeology

in:  Ben Marwick, Jade d'Alpoim Guedes, C. Michael
    Barton, Lynsey A.  Bates, Michael Baxter, Andrew Bevan, Elizabeth A.
    Bollwerk, R. Kyle Bocinsky, Tom Brughams, Alison K. Carter, Cyler Conrad,
    Daniel A. Contreras, Stefano Costa, Enrico R. Crema, Adrianne Daggett,
    Benjamin Davies, B. Lee Drake, Thomas S. Dye, Phoebe France, Richard
    Fullagar, Domenico Giusti, Shawn Graham, Matthew D. Harris, John Hawks,
    Sebastian Heath, Damien Huffer, Eric C. Kansa, Sarah Whitcher Kansa, Mark
    E. Madsen, Jennifer Melcher, Joan Negre, Fraser D. Neiman, Rachel Opitz,
    David C. Orton, Paulina Przystupa, Maria Raviele, Julien Riel-Salvatore,
    Philip Riris, Iza Romanowska, Jolene Smith, **Néhémie
    Strupler**, Isaac I. Ullah, Hannah G. Van Vlack, Nathaniel VanValkenburgh,
    Ethan C. Watrall, Chris Webster, Joshua Wells, Judith Winters et Colin D.
    Wren, SAA Archaeological Record,
    17(4), 8-14.

 -  _`Link to` [Open access publication][oa-open-sience-saa]_


### 2016

#### ‘Dater d’après le cachet’: Une approche méthodologique pour visualiser les cachets circulaires hittites

in: Butterlin P., Patrier J. and Quenet P.
    (Eds), Milles et une empreintes. Un Alsacien en Orient. Mélanges en l’honneur
    du 65e anniversaire de D. Beyer, 450-460.

 -  _`Green Open Access (Author's Manuscript):` [pdf (hal-01485166)][dater-cachets-preprint]_

#### Archaeology as Community Enterprise

in: Campana S. et al. KEEP THE
    REVOLUTION GOING! Computer Applications and Quantitative Methods in
    Archaeology (CAA) Annual Conference, Siena, March 30th to April 3rd 2015,
    1015-1018.
 -  _`Green Open Access (Author's Manuscript):` [pdf (halshs-01448857)][community-enterprise-preprint]_\
    _`Link to slideshow` [on GitLab][community-enterprise-slideshow]_

### 2013

Vorratshaltung im mittelbronzezeitlichen Boğazköy – zwischen häuslicher und regionaler Ökonomie
:    Istanbuler Mitteilungen,
     73, 15–70.\
     _`Green Open Access (Author's Manuscript):` [pdf (hal-01430408)][Vorrashaltung-preprint]_\
     _`Link to repository (data, images and paper)` [on GitLab][Vorratshaltung-repository]_

Hethitische Keramik aus den Grabungen in der Unterstadt
:    in:
     A. Schachner, *Die Arbeiten in Boğazköy – Ḫattuša 2012*, Archäologischer
     Anzeiger, 2013/2, 164–170. \
     _`Green Open Access (Author's Manuscript):` [pdf (hal-01482278)][HethKera-preprint]_

Neue Radiocarbon-Datierungen aus den Grabungen in der Unterstadt
:    in:
     A. Schachner, Die Arbeiten in *Boğazköy – Ḫattuša 2012*, Archäologischer
     Anzeiger 2013/2, 157–164.\
     _`Green Open Access (Author's Manuscript):` [pdf (hal-01481519)][Radiocarbon-preprint]_\
     _`Link to repository (data, images and paper)` [on GitLab][Radiocarbon-data]_

### 2012

Reconstitution des vases à reliefs monochromes d’Alaca Höyük et d’Eskiyapar
:    Anatolia Antiqua XX, 1–12.\
     _`Open access DOI:` [doi: 10.3406/anata.2012.1320][Reconstitution-article]_\
     _`Green Open Access (Author's Manuscript):` as [webpage][Reconstitution-preprint]_\
     _`Link to repository (data, images and paper)` [on GitLab][Reconstitution-repository]_

### 2011

Vorläufiger Überblick über die Karum-zeitlichen Keramikinventare
:    in:
     A. Schachner, *Die Ausgrabungen in Boğazköy – Ḫattuša 2010*, Archäologischer
     Anzeiger 2011/1, 51–57. \
     _`Green Open Access (Author's Manuscript):` [pdf (hal-01483327)][KarumKera-preprint]_


[dater-cachets-preprint]:         https://halshs.archives-ouvertes.fr/hal-01485166
[community-enterprise-preprint]:  https://halshs.archives-ouvertes.fr/halshs-01448857
[community-enterprise-slideshow]: https://gitlab.com/nehemie/Strupler2016a--CAA_2015/tree/master/Slideshow
[Vorratshaltung-repository]:      https://gitlab.com/nehemie/Strupler2013d-Vorratshaltung
[Vorrashaltung-preprint]:         https://hal.archives-ouvertes.fr/hal-01430408
[HethKera-preprint]:              https://hal.archives-ouvertes.fr/hal-01482278
[Radiocarbon-preprint]:           https://hal.archives-ouvertes.fr/hal-01481519
[Radiocarbon-data]:               https://gitlab.com/nehemie/Strupler2013a-Radiocarbon
[Reconstitution-article]:         http://dx.doi.org/10.3406/anata.2012.1320
[Reconstitution-preprint]:        https://nehemie.gitlab.io/Strupler2012a-Vases_Alaca_Eskiyapar/RVAE.html
[Reconstitution-repository]:      https://gitlab.com/nehemie/Strupler2012a-Vases_Alaca_Eskiyapar/tree/master/images
[KarumKera-preprint]:             https://hal.archives-ouvertes.fr/hal-01483327
[repro-inthe-field]:              https://doi.org/10.1515/opar-2017-0019
[oa-open-sience-saa]:             http://www.saa.org/Portals/0/SAA_Record_Sept_2017_Final_LR.pdf
[doi_sulbalternitaet_fka]:        https://doi.org/10.17169/refubium-30125
[doi_rediscovering_ia]:           https://doi.org/10.11141/ia.56.6


## Open Data

### 2020

Project Panormos Archaeological Survey: Satellite Image (0.2.0)

:    DOI:
     [10.5281/zenodo.3991539](https://doi.org/10.5281/zenodo.3991539)

Project Panormos Archaeological Survey Data (0.2.0)
with Wilkinson, T. C. and Slawisch, A.

:    DOI:
    [10.5281/zenodo.3941561](https://doi.org/10.5281/zenodo.3941561)

### 2018

Project Panormos Archaeological Survey: Data Visualisation Code (survey-analysis)

:    DOI:
     [10.5281/zenodo.1185024](https://doi.org/10.5281/zenodo.1185024)

Project Panormos Archaeological Satellite Image (gis-copernicus)

:    DOI:
    [10.5281/zenodo.1185044](https://doi.org/10.5281/zenodo.1185044)

### 2017

Project Panormos Archaeological Survey Data (survey-data)
with Wilkinson, T. C. & Slawisch, A.
:     DOI:
     [10.5281/zenodo.1039980](https://doi.org/10.5281/zenodo.1039980)

Project Panormos Archaeological Daily Log (survey-dailylog)
with Wilkinson, T. C. & Slawisch, A.
:     DOI:
      [10.5281/zenodo.1066023](https://doi.org/10.5281/zenodo.1066023)

Project Panormos Archaeological Survey: Photograph Archive (survey-photo-archive)
with Wilkinson, T. C. & Slawisch, A.
:     DOI:
      [10.5281/zenodo.1066039](https://doi.org/10.5281/zenodo.1066039)


## Open Notebook

### &infin;

GitLab Repository
:   <https://gitlab.com/nehemie>: Data and code from published articles,
    talks, on-going research, blog notes.




## Book reviews

### 2022

Object Biographies: Collaborative Approaches to Ancient Mediterranean Art
:   John North Hopkins, Sarah Kielt Costello et Paul R. Davis (ed.),
    Yale University Press, 2021, 240 p., Bryn Mawr
    Classical Review (ISSN : 1055-7660).\
    [\<https://bmcr.brynmawr.edu/2022/2022.01.10/\>](https://bmcr.brynmawr.edu/2022/2022.01.10/)

### 2021

La conquête du passé. Aux origines de l'archéologie
:   Alain Schnapp, Paris, La Découverte, 2020, 394 p.,Lectures (ISSN :
    2116-5289).\
    DOI:
    [10.4000/lectures.49338](https://doi.org/10.4000/lectures.49338)\
     _`Green Open Access (Author's Manuscript):` [pdf&nbsp;(hal-03444572)](https://hal.archives-ouvertes.fr/hal-03444572)_

### 2020

New Directions in Cypriot Archaeology
:   Kearns, Catherine et Sturt W. Manning (éd.), Cornell University Press, 2019. 312 p.,Bryn
    Mawr Classical Review (ISSN : 1055-7660). \
    [\<https://bmcr.brynmawr.edu/2020/2020.09.32/\>](https://bmcr.brynmawr.edu/2020/2020.09.32/)

## Magazine Articles

### 2021

L'archéologie entre héritage patrimonial et diplomatie
:    Orient XXI (June 2021):
[Link to website](https://orientxxi.info/lu-vu-entendu/l-archeologie-entre-heritage-patrimonial-et-diplomatie,4841)\
     _`Green Open Access (Author's Manuscript):` [pdf&nbsp;(hal-03445182)](https://hal.archives-ouvertes.fr/hal-03445182)_

### 2019

Turquie. Le tourisme soumis aux aléas sécuritaires
:    Orient XXI (September 2019):
[Link to website](https://orientxxi.info/magazine/turquie-le-tourisme-soumis-aux-aleas-securitaires,3265)


Tourism in Turkey at the Mercy of Security Issues
:    Orient XXI (September 2019):
[Link to website](https://orientxxi.info/magazine/tourism-in-turkey-at-the-mercy-of-security-issues,3275)

(Arabic) تركيا: السياحة رهن التقلبات الأمنية
:    Orient XXI (September 2019):
[Link to website](https://orientxxi.info/magazine/article3276)

(Farsi) کاهش گردشگري در ترکيه به خاطر عدم امنيت
:    Orient XXI (September 2019):
[Link to website](https://orientxxi.info/magazine/articles-en-persan/article3285)

## Blog posts

### 2019

Compte-rendu de la conférence de presse du projet européen mené par le Pôle Archéologie de l'IFEA à Istanbul
:    [Dipnot (n°2325)](https://dipnot.hypotheses.org/2325)


Le musée Saint-Irène à Constantinople
:    [Dipnot (n°2285)](https://dipnot.hypotheses.org/2285)


### 2017

The world in pixels
:    [nehemie.gitlab.io](https://nehemie.gitlab.io/2017/03/25/Hyperspectral/)


Open Source Software Adds to Collaboration, Transparency and Reproducibility in Archaeology
:    [openscience.com](http://openscience.com/open-source-software-contributes-to-project-collaboration-research-transparency-and-reproducibility-in-archeology/)\
blog post covering the article Strupler & Wilkinson 2017 by Pablo Markin

### 2016

It's Open Access Week!
:    [ANAMED blog: http://anamedblog.tumblr.com](http://anamedblog.tumblr.com/post/152239961063/its-open-access-week)

Aperçu du catalogue de la bibliothèque de l'IFEA
:    [Dipnot (n°1961](https://dipnot.hypotheses.org/1961)

L'apparition du palais en Anatolie
:    [Dipnot (n°2007)](https://dipnot.hypotheses.org/2007)

## Other artefacts

### Software

'rkeos' A collection of functions to tide, weight and map data collected during archaeological surveys with [R]
:    [https://gitlab.com/rkeos/rkeos](https://gitlab.com/rkeos/rkeos)



